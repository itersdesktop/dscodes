import requests
import urllib.request
import time
from bs4 import BeautifulSoup


def main():
    print("Tung Nguyen")
    bmscholar_url = 'https://scholar.google.co.uk/citations?hl=en&user=sxPul0AAAAAJ&view_op=list_works&sortby=pubdate'
    response = requests.get(bmscholar_url)
    text = open("bmscholar_20190726.txt", "r").readlines()[0]
    #text = response.text
    soup = BeautifulSoup(text, "html.parser")
    table_publications = soup.find('table', id='gsc_a_t')
    row_publications = table_publications.find('tbody').find_all('tr')
    for row in row_publications:
        cells = row.find_all("td")
        title = cells[0].get_text()
        cited_by = cells[1].get_text()
        year = cells[2].get_text()
        if title.find("BioModels") >= 0:
            print(title)
            print(cited_by, year)


if __name__ == '__main__':
    main()
