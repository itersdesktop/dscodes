import urllib3
from bs4 import BeautifulSoup

def get_upcoming_events(url):
    req = urllib3.PoolManager()
    res = req.request('GET', url)

    soup = BeautifulSoup(res.data, 'html.parser')

    events = soup.find('ul', {'class': 'list-recent-events'}).findAll('li')

    for event in events:
        event_details = dict()
        event_details['name'] = event.find('h3').find("a").text
        event_details['location'] = event.find('span', {'class', 'event-location'}).text
        event_details['time'] = event.find('time').text
        print(event_details)


def test_links_memories(file_name):
    with open(file_name, "r") as f:
        contents = f.read()
        soup = BeautifulSoup(contents, 'lxml')
        posts = soup.findAll('div', {'class': '_37i-'})
        # posts = soup.findAll(attrs={"data-ft" : "data-ft"})
        for i in range(0, len(posts)-1):
            p = posts[i]
            sp = BeautifulSoup(p, 'lxml')
            # data_ft = sp.findAll(attrs={"name" : "data-ft"})
            print(p)

#get_upcoming_events('https://www.python.org/events/python-events/')
#test_links_memories('https://www.facebook.com/memories/?source=bookmark')
test_links_memories('/Users/tnguyen/Drive/nguyenvungoctung/DownloadFBMemories/20191111.txt')