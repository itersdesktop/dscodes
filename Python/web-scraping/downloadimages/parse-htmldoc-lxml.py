from lxml.etree import ParserError
from lxml import etree
from multiprocessing.pool import ThreadPool
import requests
import time
import os.path

start = time.time()


def basic_test():
    # Example html content
    html = '''<div class="container"> 
    <p class="row"> 
    <a href="#123333" class="box"> I love xpath </a> 
    </p> 
    </div>'''

    # Use etree to process html text and return an _Element object which is a dom object.
    dom = etree.HTML(html)

    # Get a tag's text. Please Note: The _Element's xpath method always return a list of html nodes. Because there is
    # only one a tag's text, so we can do like below.
    a_tag_text = dom.xpath('//div/p/a/text()')

    print(a_tag_text)

    root_elem = etree.Element('html')
    etree.SubElement(root_elem, 'head')
    etree.SubElement(root_elem, 'title')
    etree.SubElement(root_elem, 'body')
    print(etree.tostring(root_elem, pretty_print=True).decode("utf-8"))

    html = root_elem[0]
    print(html.tag)

    for e in root_elem:
        print(e.tag)

    html_text = '''
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <title>Page Title</title>
    </head>
    <body>

    <h1>This is a Heading</h1>
    <p>This is a paragraph.</p>

    </body>
    </html>
    '''
    html = etree.XML(html_text)
    print(etree.tostring(html, pretty_print=True).decode('utf-8'))


def parse_html_doc(_html_doc):
    _html_dom = None
    try:
        _parser = etree.HTMLParser()
        # html_dom = etree.parse(StringIO(html_doc), parser)
        _html_dom = etree.HTML(_html_doc, _parser)
    except ParserError as e:
        print(e)
    return _html_dom


def extract_images_urls(_urls):
    if _urls:
        extracted_images_urls = []
        for e in _urls:
            # print(e)
            response = requests.get(e, headers=headers)
            html_dom = parse_html_doc(response.content)
            images = html_dom.xpath('//a[contains(@href, "wallpapers/1920_1200")]/@href')
            if len(images):
                extracted_images_urls.append(images[0])
    return extracted_images_urls


def download_url(_url):
    print("downloading: ", _url)
    # assumes that the last segment after the / represents the file name
    # if url is abc/xyz/file.jpg, the file name will be file.jpg
    file_name_start_pos = _url.rfind("/") + 1
    file_name = _url[file_name_start_pos:]

    r = requests.get(_url, stream=True)
    if r.status_code == requests.codes.ok:
        with open(file_name, 'wb') as f:
            for data in r:
                f.write(data)
    return os.path.exists(file_name)


def run_download_sequential(_urls):
    if _urls:
        for e in _urls:
            if e is not None:
                is_ok = download_url(e)
                if not is_ok:
                    print('failed to download %s'.format(e))


def run_download_parallel(_urls):
    # Run 5 multiple threads. Each call will take the next element in urls list
    results = ThreadPool(5).imap_unordered(download_url, _urls)
    for r in results:
        print(r)


url = "https://all-free-download.com/wallpapers/audi-a4-2016.html"
headers = {'Content-Type': 'text/html', }
response = requests.get(url, headers=headers)
html_doc = response.content
html_dom = parse_html_doc(html_doc)
urls = html_dom.xpath(
    '//div[@class="imgcontainer"]/div[@class="item"]/a[contains(@href, "download") and contains(@href, "audi")]/@href')
images_urls = extract_images_urls(urls)
print(len(images_urls))

run_download_sequential(images_urls)
# run_download_parallel(images_urls)

end = time.time()
print('Time taken to download {}'.format(str(len(urls))))
print(end - start)