Data Science
https://www.datacamp.com/home
https://www.youtube.com/channel/UCfzlCWGWYyIQ0aLC5w48gBQ
https://www.safaribooksonline.com/learning-paths/learning-path-data/9781789619027/9781785880711-video1_3?autoplay=false
https://www.youtube.com/watch?v=THODdNXOjRw
https://www.futurelearn.com/courses/big-data/2/steps/58968
https://classroom.udacity.com/courses/ud359/lessons/649959144/concepts/6381707760923

Python
https://medium.freecodecamp.org/an-a-z-of-useful-python-tricks-b467524ee747
https://www.udemy.com/data-analysis-with-pandas/?start=0
http://maxmelnick.com/2016/04/19/python-beginner-tips-and-tricks.html
https://hackernoon.com/top-10-python-web-frameworks-to-learn-in-2018-b2ebab969d1a

Django
https://www.djangoproject.com/

Flask
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-ix-pagination
http://flask.pocoo.org/docs/1.0/quickstart/
http://flask.pocoo.org/docs/1.0/tutorial/layout/

Machine Learning
https://machinelearningcoban.com/2018/07/06/deeplearning/

git clean
https://git-scm.com/docs/git-clean
https://koukia.ca/how-to-remove-local-untracked-files-from-the-current-git-branch-571c6ce9b6b1
https://sethrobertson.github.io/GitFixUm/fixup.html
https://coderwall.com/p/9idt5g/keep-your-feature-branch-up-to-date
https://robots.thoughtbot.com/write-good-commit-messages-by-blaming-others
https://ardalis.com/why-delete-old-git-branches
https://ardalis.com/how-to-make-git-forget-tracked-files-in-gitignore
https://askjong.com/howto/use-local-repository-with-composer
https://coderwall.com/p/euwpig/a-better-git-log
https://stackoverflow.com/questions/424071/how-to-list-all-the-files-in-a-commit?answertab=active#tab-top
https://www.lullabot.com/articles/git-best-practices-upgrading-the-patch-process


Builder Pattern
https://jlordiales.me/2012/12/13/the-builder-pattern-in-practice/
https://www.javaworld.com/article/2074938/core-java/too-many-parameters-in-java-methods-part-3-builder-pattern.html?page=2


log4j2
https://crunchify.com/error-statuslogger-no-log4j2-configuration-file-found-using-default-configuration-logging-only-errors-to-the-console/
https://stackify.com/log4j2-java/
https://stackoverflow.com/questions/25487116/log4j2-configuration-no-log4j2-configuration-file-found
https://logging.apache.org/log4j/2.x/manual/configuration.html
https://dzone.com/articles/log4j-2-configuration
https://www.boraji.com/log4j-2-yaml-configuration-example

Progress Bar with AJAX
https://stackoverflow.com/questions/39777837/progress-bar-for-file-uploads-to-php-using-ajax-and-bootstrap?rq=1
https://stackoverflow.com/questions/20095002/how-to-show-progress-bar-while-loading-using-ajax
http://www.dave-bond.com/blog/2010/01/JQuery-ajax-progress-HMTL5/
https://stackoverflow.com/questions/1964839/how-can-i-create-a-please-wait-loading-animation-using-jquery


jQuery with ComboBox
https://bytutorial.com/blogs/jquery/how-to-get-selected-combo-box-value-using-jquery-and-javascript
https://www.scriptol.com/html5/combobox.php
https://www.jqwidgets.com/jquery-widgets-demo/demos/jqxcombobox/index.htm#demos/jqxcombobox/bindingtoxml.htm
https://www.jqwidgets.com/jquery-widgets-demo/demos/jqxcombobox/index.htm

datalist
https://www.html.am/tags/html-datalist-tag.cfm

https://github.com/denysdovhan/wtfjs

ReactJS
http://www.mattmorgante.com/technology/dropdown-with-react


ARIA
https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA

JACKSON exceptions
https://www.baeldung.com/jackson-exception
https://www.tutorialspoint.com/jenkins/jenkins_setup_build_jobs.htm


Saving data into JSON with jquery
https://www.encodedna.com/jquery/read-json-file-push-data-into-array-and-convert-to-html-table-using-jquery.htm


GRADLE
https://docs.gradle.org/4.0/userguide/plugins.html#sec:plugins_block
https://gradle.org/guides/
http://www.mkyong.com/gradle/how-to-use-gradle-wrapper/
https://www.baeldung.com/gradle-fat-jar
https://dzone.com/articles/java-8-how-to-create-executable-fatjar-without-ide
https://openliberty.io/guides/gradle-intro.html

Consuming a RESTful Web Service
https://spring.io/guides/gs/consuming-rest/

JENKINS
https://www.mkyong.com/gradle/jenkins-could-not-find-gradlewrappermain/

Bash Script
https://stackoverflow.com/questions/255414/why-doesnt-cd-work-in-a-shell-script
https://stackoverflow.com/questions/3879431/how-to-run-cd-in-shell-script-and-stay-there-after-script-finishes

Upgrading JUMMP from grails 2 to 3
Webflow
https://github.com/grails/grails-core/issues/601
http://projects.spring.io/spring-webflow/#quick-start
https://examples.javacodegeeks.com/enterprise-java/spring/web-flow/spring-web-flow-tutorial/
https://www.isostech.com/blogs/software-development/spring-webflow-good-good-bad/
https://www.isostech.com/blogs/devops/vagrant-virtualbox-vs-vmware-fusion/

jquery multi step form example
multi step form jquery bootstrap
multi step form with progress bar bootstrap
jquery-multi-step-form-with-progress-bar
https://www.formget.com/multi-step-form-using-jquery-and-css3/
https://djaodjin.com/blog/jquery-multi-step-validation.blog.html
http://thecodeplayer.com/walkthrough/jquery-multi-step-form-with-progress-bar
https://codepen.io/atakan/pen/gqbIz
http://talkerscode.com/webtricks/multistep-form-with-progress-bar-using-jquery-and-css.php


http://pbpython.com/pdf-splitter-gui.html


https://www.wjunction.com/
https://eloquentjavascript.net/15_event.html
https://www.quickanddirtytips.com/books
https://portal.gdc.cancer.gov/
https://www.worldlifestyle.com/featured/hilarious-airport-moments/4/
http://affiliate.monkeyjunior.vn/
https://pirawen.blogspot.com/2018/10/java-vs-python-comparison.html

Update the Curation Notes
https://coderwall.com/p/muwupg/datepicker-with-time-timepicker-jquery-ui