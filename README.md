# Introduction


# Data Mining Techniques for Recommender System

# Recommender System
A practical introduction to recommender system
https://cambridgespark.com/practical-introduction-to-recommender-systems/
Introduction: Collaborative Filtering
https://www.ethanrosenthal.com/2015/11/02/intro-to-collaborative-filtering/
Various implementations of collaborative filtering
https://towardsdatascience.com/various-implementations-of-collaborative-filtering-100385c6dfe0
# Machine Learning
## Split dataset
## Select features
## Train algorithm
## Evaluate algorithm
# Deep Learning

# Machine Learning Algorithms
This section summarised the most common algorithms used in Machine Learning
## Supervised Classification
### Random Forest and Decision Tree
Introduction and How it works
http://dataaspirant.com/2017/05/22/random-forest-algorithm-machine-learing/
How decision tree works
http://dataaspirant.com/2017/01/30/how-decision-tree-algorithm-works/

## Clustering

# References
https://github.com/ntung/100-Days-Of-ML-Code
https://github.com/WillKoehrsen/Machine-Learning-Projects
https://github.com/hunkim/DeepLearningZeroToAll
https://www.cs.bham.ac.uk/~slb/courses/Taxonomy/Taxonomy02.html
https://machinelearningmastery.com/supervised-and-unsupervised-machine-learning-algorithms/
https://www.mapleprimes.com/maplesoftblog/209354-A-Beginners-Guide-To-Using-The-DNN
http://www.cs.toronto.edu/~hinton/coursera_lectures.html
http://www.cs.toronto.edu/~hinton/

Data Sources
https://data.humdata.org/dataset/world-bank-environment-indicators-for-vietnam